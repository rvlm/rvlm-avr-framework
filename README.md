Rvlm AVR framework
==================

Template-driven C++-only framework for efficient firmware development for
Atmel AVR microcontrollers with GCC AVR compiler. It tries to provide a cleaner
API then traditional GCC, without so heavy usage of preprocessor.


Prerequisites
-------------

* GCC-AVR
* CMake
* Perl (with PCAN modules: DateTime)
* Doxygen
* Git
* AStyle
* Qt Creator (optional)

Quick installing
----------------

$ sudo apt-get install gcc-avr binutils-avr gdb-avr avr-libc avrdude
$ sudo cpan install DateTime
