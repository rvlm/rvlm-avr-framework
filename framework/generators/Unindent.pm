
sub unindent {
    my $str = shift;
    $str =~ s/^\s*$//mg;
    $str =~ s/^(\s*\S+\s)//mg;
    return $str;
}

1;
