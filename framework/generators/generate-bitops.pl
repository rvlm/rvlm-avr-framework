#!/usr/bin/env perl
use strict;
use warnings;

my ($targetFile, $maxargs) = @ARGV;
open(TARGET, ">", $targetFile) or die "Unable to open target file.";
int($maxargs) > 1 or die "Wrong command line options given.";

print TARGET (<<EOF);
#pragma once
#include <rvlm/avr/stdint.hh>

// WARNING:
// This file contains generated code!
// Please do not modify its contents since all your changes will be lost
// on regeneration. Modify generate-bitopts.pl file instead.
//
// NOTE:
// This file's lines length is not restricted to 80 columns. Do not try
// to beautify out of pure perfectionism.

EOF

foreach my $argn (2..$maxargs) {
    my @args = (1..$argn);
    my $declaration = join ", ", (map "const int bit$_", @args);
    my $enumeration = join ", ", (map "bit$_",           @args);
    my $maskshifts  = join "|",  (map "(1 << bit$_)",    @args);

print TARGET (<<EOF);
// Bit operations with $argn bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask($declaration)
{
    return $maskshifts;
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, $declaration) {
    return !isSetBit(value, $enumeration);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, $declaration) {
    return value & bitmask<IntegralType>($enumeration) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, $declaration) {
    IntegralType mask = ~bitmask<IntegralType>($enumeration);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, $declaration) {
    IntegralType mask = bitmask<IntegralType>($enumeration);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, $declaration) {
    value |= bitmask<IntegralType>($enumeration);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, $declaration) {
    value &= ~bitmask<IntegralType>($enumeration);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, $declaration) {
    value ^= bitmask<IntegralType>($enumeration);
}

} // namespace avr
} // namespace rvlm

EOF
}
