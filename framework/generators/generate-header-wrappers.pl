#!/usr/bin/env perl
use strict;
use warnings;
use File::Basename        qw/basename dirname/;
use File::Spec::Functions qw/abs2rel rel2abs/;
use File::Find;
use DateTime;

use lib dirname(__FILE__);
use Unindent;

my $scriptName = basename(__FILE__);
my $baseDir = shift;
$baseDir or die("Not enough parameters given.");

# Remove recursively already present files without extension.
print find( sub { unlink $_ if $_ =~ /^[^.]+$/ }, $baseDir);

foreach my $file (@ARGV) {
    $file = rel2abs($file);

    my $targetFile = $file;
       $targetFile =~ s/\.\w+$//;
    my $includeRec = abs2rel($file, $baseDir);

    open(TARGET, '>', $targetFile) or die "Cannot open file: $targetFile";
    my $baseName = basename($file);
    my $dateTime = DateTime->now->iso8601();
    print TARGET unindent "
        | /*
        |  * WARNING: This file contains generated code!
        |  * Please do not modify its contents since all your changes will
        |  * be lost on regeneration.
        |  * Modify instead: $scriptName, $baseName
        |  */
        | #pragma once
        | #include <$includeRec>
        | ";
    close(TARGET);
}
