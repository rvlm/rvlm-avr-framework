#!/usr/bin/env perl
use strict;
use warnings;
use File::Basename;

my ($macroName, $directory, @files) = @ARGV;
$macroName && $directory or die "Wrong command line options are given.";

print <<EOF;
#pragma once

// WARNING:
// This file contains generated code!
// Please do not modify its contents since all your changes will be lost
// on regeneration. Modify generate-select-header.pl file instead.
//
// NOTE:
// This file's lines length is not restricted to 80 columns. Do not try
// to beautify it out of pure perfectionism.

#ifndef $macroName
#error "You must define $macroName before including this file."
#else
#if 0
EOF

foreach my $file (@files) {
my ($mcuName, $dir, $suffix) = fileparse $file, qr/\.[^.]*/;
print <<EOF;
#elif $macroName == "$mcuName"
#include <$directory$mcuName$suffix>
EOF
}

print <<EOF;
#else
#error "Unsupported value of $macroName."
#endif
#endif
EOF

