#!/usr/bin/env perl
use warnings;
use strict;
use Data::Dumper;
use IPC::Open2;

my ($filename, $device) = @ARGV;
open(TARGET, ">", $filename)
    or die("Unable to open target file.");

open2(\*IN, \*OUT, "avr-cpp -dM --include=avr/io.h -D__AVR_${device}__")
    or die "Cannot invoke subprocess.";

#
close(OUT);

my %ints  = ();
my %io8   = ();
my %io16  = ();
my %mem8  = ();
my %mem16 = ();
my %fuse  = ();
my %rest  = ();
my $efuse = undef;
my $lfuse = undef;
my $hfuse = undef;
my $lockbits = undef;

while (<IN>) {
    chomp;
    next unless /^#\s*define\s+(\w[\w\d]*)\s+(.+)$/;

    my $name = $1;
    my $code = $2;

    # Skip lots of standard definitions.
    next if $name =~ /^_+/;
    next if $name =~ /^SCN/;
    next if $name =~ /^PRI/;
    next if $name =~ /^U?INT[\w\d]+_(MAX)|(MIN)$/;
    next if $name =~ /^PTRDIFF[\w\d]*_(MAX)|(MIN)$/;
    next if $code =~ /^U?INT[\w\d]+_(MAX)|(MIN)$/;
    next if $code =~ /^PTRDIFF[\w\d]*_(MAX)|(MIN)$/;

    # Skip some AVR-specific things.
    next if $name =~ /^(INTF?|EPRST|PORT|PIN\w?|DD\w?|P\w)(\d)$/ and $2 == $code;
    next if $name =~ /^XRAMEND$/;
    next if $name =~ /AVR_?(STATUS|STACK|RAMPZ)?/;
    next if $code =~ /^r\d+$/;
    next if $code =~ /^P\w\d$/;

    # It macro value is just an integral number (in dec, oct or hex), most likely
    # it is a pin number. Convert it to constant.
    if ($code =~ /^\(?(\d+)|(0x[a-fA-F\d]+)\)?$/) {
        $rest{$name} = eval($code);
        next;
    }

    if ($code =~ /^_SFR_MEM8\((.+)\)$/) {
        $mem8{$name} = eval($1);
        next;
    }

    if ($code =~ /^_SFR_MEM16\((.+)\)$/) {
        $mem16{$name} = eval($1);
        next;
    }


    if ($code =~ /^_SFR_IO8\((.+)\)$/) {
        $io8{$name} = eval($1);
        next;
    }

    if ($code =~ /^_SFR_IO16\((.+)\)$/) {
        $io16{$name} = eval($1);
        next;
    }

    if ($code =~ /^_VECTOR\((\d+)\)$/) {
        $ints{$name} = eval($1);
        next;
    }

    if ($code =~ /^\(unsigned char\)~_BV\((\d)\)$/) {
        $fuse{$name} = eval($1);
        next;
    }

    if ($name eq "EFUSE_DEFAULT") {
        $efuse = $code;
        next;
    }

    if ($name eq "LFUSE_DEFAULT") {
        $lfuse = $code;
        next;
    }

    if ($name eq "HFUSE_DEFAULT") {
        $hfuse = $code;
        next;
    }

    if ($name eq "LOCKBITS_DEFAULT") {
        $lockbits = $code;
        next;
    }

    print STDERR "Unrecognized: $_\n";
}

$Data::Dumper::Sortkeys  = 1;
$Data::Dumper::Indent    = 1;
$Data::Dumper::Quotekeys = 0;
print TARGET Data::Dumper->Dump([{
    "ints"  => \%ints,
    "io8"   => \%io8,
    "io16"  => \%io16,
    "mem8"  => \%mem16,
    "fuse"  => {
        "bits" => \%fuse,
        "edefault" => $efuse,
        "ldefault" => $lfuse,
        "hdefault" => $hfuse },
    "rest"  => \%rest,
    "lockbits" => $lockbits}], ["*device"])

