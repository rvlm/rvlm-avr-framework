#pragma once

/**
 * @defgroup intrinsics Compiler intrinsics
 * This module contains function which provides direct access to low-level
 * feature and assembler instructions. Note that all of them have to be always
 * inlined because both their performance and correctness heavily depend
 * on that, so usage of @c RVLM_AVR_FORCE_INLINE macro is allowed here.
 */

namespace rvlm {
namespace avr {
namespace intrinsics {

/**
 * @ingroup intrinsics
 * Compiler memory barrier.
 * Ensures that all prior memory access operations in source code are actually
 * performed before barrier. This intrinsic may impact the way how compiler
 * optimizes source code, so it must be used with caution.
 */
RVLM_AVR_FORCE_INLINE
inline void memoryBarrier() {
    __asm__ __volatile__ ("" ::: "memory");
}

/**
 * @ingroup intrinsics
 * AVR assembler instruction "nop".
 * Doesn't do anything but wasting program space and processor time. But it
 * can be useful in some cases, though.
 */
RVLM_AVR_FORCE_INLINE
inline void nop() {
    __asm__ __volatile__ ("nop");
}

/**
 * @ingroup intrinsics
 * AVR assembler instruction "cli".
 * Clears @c SREG's global interrupts flag, effectively disabling handling
 * of all hardware interrupts. This instrinsic also implies memory barrier.
 * @see sei
 */
RVLM_AVR_FORCE_INLINE
inline void cli() {
    __asm__ __volatile__ ("cli" ::: "memory");
}

/**
 * @ingroup intrinsics
 * AVR assembler instruction "sei".
 * Sets @c SREG's global interrupts flag, effectively enabling handling
 * of all hardware interrupts. This instrinsic also implies memory barrier.
 * @see sei
 */
RVLM_AVR_FORCE_INLINE
inline void sei() {
    __asm__ __volatile__ ("sei" ::: "memory");
}

/**
 * @ingroup intrinsics
 * AVR assembler instruction "wdr".
 * Resets watchdog timer letting it know that the program is operating properly
 * and hasn't hanged yet.
 */
RVLM_AVR_FORCE_INLINE
inline void wdr() {
    __asm__ __volatile__ ("wdr");
}

/**
 * @ingroup intrinsics
 * AVR assembler instruction "sleep".
 * Turns processor into power-saving operational mode until it is woken up by
 * an interrupt. This instrinsics also implies memory barrier to make memory
 * state consistent while processor is asleep.
 */
RVLM_AVR_FORCE_INLINE
inline void sleep() {
    __asm__ __volatile__ ("sleep" ::: "memory");
}

/**
 * @ingroup intrinsics
 * AVR assembler instruction "break".
 * Make processor stop working and acts as a debugger breakpoint. This
 * instrinsics also implies memory barrier to provide debugger with actual
 * memory data and not to mislead the programmer.
 *
 * @note
 * Function name has different pattern from other intrisics because @c break
 * is a reserved word in C and C++.
 */
RVLM_AVR_FORCE_INLINE
inline void breakOp() {
    __asm__ __volatile__ ("break" ::: "memory");
}

} // namespace intrinsics
} // namespace avr
} // namespace rvlm
