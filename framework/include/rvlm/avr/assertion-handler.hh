#pragma once
#include <rvlm/avr/assertion-handler-fwd.hh>
#include <rvlm/avr/assertion-handler-impl.hh>

namespace rvlm {
namespace avr {

/**
 * @ingroup assertions
 * Returns currently set assertion handler.
 * In case no custom assertion handler was set before calling this function,
 * the pointer to default handler is returned.
 * @see setAssertionHandler
 * @see AssertionHandler
 */
inline const AssertionHandler& getAssertionHandler() {
    return impl::getAssertionHandler();
}

/**
 * @ingroup assertions
 * Sets custom assertion handler.
 * This handler is a pointer to function which will be called when a failed
 * assertion occurs.
 * @see getAssertionHandler
 * @see AssertionHandler
 */
inline void setAssertionHandler(const AssertionHandler& handler) {
    impl::setAssertionHandler(handler);
}

} // namespace avr
} // namespace rvlm
