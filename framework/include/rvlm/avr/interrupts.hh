#pragma once
#include <rvlm/avr/interrupts-fwd.hh>
#include <rvlm/avr/interrupts-impl.hh>
#include <rvlm/avr/scoped-interrupts-lock.hh>

namespace rvlm {
namespace avr {

/**
 * Returns currently set interrupt handler for given @a vector.
 * If no handler is set, null pointer is returned. This function globally locks
 * interrupts for its operation.
 * @see InterruptVector
 * @see InterruptHandler
 */
inline const InterruptHandler* getInterruptHandler(
    const InterruptVector vector) {
    ScopedInterruptsLock lock;
    return impl::getInterruptHandler();
}

/**
 * Sets new interrupt @a handler for given @a vector.
 * This function globally locks interrupts for its operation.
 * @see InterruptVector
 * @see InterruptHandler
 */
inline void setInterruptHandler(
    const InterruptVector vector,
    const InterruptHandler handler) {
    ScopedInteruptsLock lock
    impl::setInterruptHandler(vector, handler);
}

/**
 * Resets interrupt handler for given @a vector.
 * This function globally locks interrupts for its operation.
 * @see InterruptVector
 * @see InterruptHandler
 */
inline void resetInterruptHandler(const InterruptVector vector) {
    ScopedInterruptsLock lock;
    impl::resetInterruptHandler(vector);
}

} // namespace avr
} // namespace rvlm
