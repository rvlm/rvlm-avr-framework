#pragma once
#include <rvlm/avr/stdint.hh>

// WARNING:
// This file contains generated code!
// Please do not modify its contents since all your changes will be lost
// on regeneration. Modify generate-bitopts.pl file instead.
//
// NOTE:
// This file's lines length is not restricted to 80 columns. Do not try
// to beautify out of pure perfectionism.

// Bit operations with 2 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2) {
    return (1 << bit1)|(1 << bit2);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2) {
    return !isSetBit(value, bit1, bit2);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2) {
    return value & bitmask<IntegralType>(bit1, bit2) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2) {
    value |= bitmask<IntegralType>(bit1, bit2);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2) {
    value &= ~bitmask<IntegralType>(bit1, bit2);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2) {
    value ^= bitmask<IntegralType>(bit1, bit2);
}

} // namespace avr
} // namespace rvlm

// Bit operations with 3 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2, const int bit3) {
    return (1 << bit1)|(1 << bit2)|(1 << bit3);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2, const int bit3) {
    return !isSetBit(value, bit1, bit2, bit3);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2, const int bit3) {
    return value & bitmask<IntegralType>(bit1, bit2, bit3) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2, const int bit3) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2, bit3);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2, const int bit3) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2, bit3);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3) {
    value |= bitmask<IntegralType>(bit1, bit2, bit3);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3) {
    value &= ~bitmask<IntegralType>(bit1, bit2, bit3);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2,
                     const int bit3) {
    value ^= bitmask<IntegralType>(bit1, bit2, bit3);
}

} // namespace avr
} // namespace rvlm

// Bit operations with 4 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2, const int bit3,
                            const int bit4) {
    return (1 << bit1)|(1 << bit2)|(1 << bit3)|(1 << bit4);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4) {
    return !isSetBit(value, bit1, bit2, bit3, bit4);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4) {
    return value & bitmask<IntegralType>(bit1, bit2, bit3, bit4) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2, bit3, bit4);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2, bit3, bit4);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4) {
    value |= bitmask<IntegralType>(bit1, bit2, bit3, bit4);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4) {
    value &= ~bitmask<IntegralType>(bit1, bit2, bit3, bit4);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2,
                     const int bit3, const int bit4) {
    value ^= bitmask<IntegralType>(bit1, bit2, bit3, bit4);
}

} // namespace avr
} // namespace rvlm

// Bit operations with 5 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2, const int bit3,
                            const int bit4, const int bit5) {
    return (1 << bit1)|(1 << bit2)|(1 << bit3)|(1 << bit4)|(1 << bit5);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5) {
    return !isSetBit(value, bit1, bit2, bit3, bit4, bit5);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5) {
    return value & bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5) {
    value |= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5) {
    value &= ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2,
                     const int bit3, const int bit4, const int bit5) {
    value ^= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5);
}

} // namespace avr
} // namespace rvlm

// Bit operations with 6 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2, const int bit3,
                            const int bit4, const int bit5, const int bit6) {
    return (1 << bit1)|(1 << bit2)|(1 << bit3)|(1 << bit4)|(1 << bit5)|(1 << bit6);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5,
                        const int bit6) {
    return !isSetBit(value, bit1, bit2, bit3, bit4, bit5, bit6);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5,
                        const int bit6) {
    return value & bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5,
                          const int bit6) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5,
                          const int bit6) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5, const int bit6) {
    value |= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5, const int bit6) {
    value &= ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2,
                     const int bit3, const int bit4, const int bit5, const int bit6) {
    value ^= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6);
}

} // namespace avr
} // namespace rvlm

// Bit operations with 7 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2, const int bit3,
                            const int bit4, const int bit5, const int bit6, const int bit7) {
    return (1 << bit1)|(1 << bit2)|(1 << bit3)|(1 << bit4)|(1 << bit5)|(1 << bit6)|
           (1 << bit7);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                        const int bit7) {
    return !isSetBit(value, bit1, bit2, bit3, bit4, bit5, bit6, bit7);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                        const int bit7) {
    return value & bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6,
                                         bit7) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                          const int bit7) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6,
                        bit7);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                          const int bit7) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6,
                        bit7);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5, const int bit6,
                    const int bit7) {
    value |= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5, const int bit6,
                    const int bit7) {
    value &= ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2,
                     const int bit3, const int bit4, const int bit5, const int bit6,
                     const int bit7) {
    value ^= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7);
}

} // namespace avr
} // namespace rvlm

// Bit operations with 8 bit(s) positions.

namespace rvlm {
namespace avr {

template <typename IntegralType>
inline IntegralType bitmask(const int bit1, const int bit2, const int bit3,
                            const int bit4, const int bit5, const int bit6, const int bit7,
                            const int bit8) {
    return (1 << bit1)|(1 << bit2)|(1 << bit3)|(1 << bit4)|(1 << bit5)|(1 << bit6)|
           (1 << bit7)|(1 << bit8);
}

template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                        const int bit7, const int bit8) {
    return !isSetBit(value, bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8);
}

template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1,
                        const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                        const int bit7, const int bit8) {
    return value & bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7,
                                         bit8) != 0;
}

template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                          const int bit7, const int bit8) {
    IntegralType mask = ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6,
                        bit7, bit8);
    return value | mask == mask;
}

template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1,
                          const int bit2, const int bit3, const int bit4, const int bit5, const int bit6,
                          const int bit7, const int bit8) {
    IntegralType mask = bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6,
                        bit7, bit8);
    return value & mask == mask;
}

template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5, const int bit6, const int bit7,
                    const int bit8) {
    value |= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8);
}

template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, const int bit2,
                    const int bit3, const int bit4, const int bit5, const int bit6, const int bit7,
                    const int bit8) {
    value &= ~bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8);
}

template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, const int bit2,
                     const int bit3, const int bit4, const int bit5, const int bit6, const int bit7,
                     const int bit8) {
    value ^= bitmask<IntegralType>(bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8);
}

} // namespace avr
} // namespace rvlm

