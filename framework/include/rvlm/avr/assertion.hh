#pragma once
#include <rvlm/avr/assertion-handler-fwd.hh>
#include <rvlm/avr/assertion-handler.hh>

namespace rvlm {
namespace avr {

/**
 * @defgroup assertions Assertions handling
 * @details
 * Assertions represent the assumptions which must be met in order the program
 * to operate properly. If they aren't, it would indicate that the programmer
 * made a mistake somewhere and the the program's state is somewhat different
 * from the expectations. So, assertions are just a matter for programmer to
 * control himself.
 */

/**
 * @ingroup assertions
 * Checks an assertion.
 * If @a statement condition isn't met, terminates the program. The way how
 * it is "terminated" can be specified using @c setAssertHandler function.
 * Default handler tries to stop the execution flow with @c BREAK assembler
 * instruction.
 *
 * Please note, that assertions should only be accessible in debug builds.
 * Define @c NDEBUG macro to compile all assertions out.
 */
inline void assert(bool statement, const char* message = 0);

#ifndef NDEBUG
void assert(bool statement, const char* message) {
    if (!statement)
        getAssertHandler()->(message);
}
#else
void assert(bool statement, const char* message) {
    (void)statement;
    (void)message;
    ; // empty function body.
}
#endif

} // namespace avr
} // namespace rvlm
