#pragma once

namespace rvlm {
namespace avr {

/**
 * @ingroup assertions
 * Pointer to assertion handler function.
 * This is a signature for a custom assertion handler, which may be set using
 * @c setAssertHandler() function. Once it was set, it will be called when
 * an assertion fails. Speaking in general, handler function *must not* return,
 * because this would spoil the whole idea of assertions. But this custom
 * handler may return, because it will be assured by the caller anyway.
 * Handler function also takes only one parameter @a message which is the
 * message provided to @c assert() as its second argument, can be @c NULL if
 * no message was provided.
 */
typedef void (*AssertionHandler)(const char* message);

} // namespace avr
} // namespace rvlm
