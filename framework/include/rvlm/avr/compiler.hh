#pragma once

#ifdef DOXYGEN

/**
 * @todo Write documentation.
 */
#define RVLM_AVR_WEAK_LINKAGE

/**
 * Compiler-specific attributes to force function inlining.
 * Cause in most cases compiler knows better whether to inline function or not,
 * this macro must be used very carefully. Generally speaking, it is almost
 * forbidden to use it everywhere but in situations where having function not
 * inlined is not only hits performance, but changes its effects. It is used
 * as follows:
 * @code
 *     RVLM_AVR_FORCE_INLINE
 *     inline void memoryBarrier() { ... }
 * @code
 */
#define RVLM_AVR_FORCE_INLINE
#endif

#ifdef __GNUG__
#define RVLM_AVR_WEAK_LINKAGE __attribute__((__weak__))
#define RVLM_AVR_FORCE_INLINE __attribute__((__always_inline__))
#endif
