/* Copyright (c) 2002,2004,2005 Marek Michalkiewicz
   Copyright (c) 2005           Carlos Lamas
   Copyright (c) 2005,2007      Joerg Wunsch
   Copyright (c) 2013           Pavel Kretov
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.

   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */

/**
 * @file stdint.hh
 * This is a modified version of stdint.h header file which is distributed with
 * GCC-AVR. (And their copyrights are retained in source code, with one more
 * line added.)
 *
 * Currently none of the following is implemented here (because it was not
 * implemented in original gcc-avr header file too):
 *
 *   - typedef wchar_t
 *   - typedef wint_t
 *   - const WCHAR_MAX
 *   - const WCHAR_MIN
 *   - const WINT_MAX
 *   - const WINT_MIN
 */
#pragma once

/*
 * At this point it was decided not to deal with GCC option -mint8,
 * because it seems to be quite dangerous: eitght bits may be not enough for
 * every use case of 'int'. Moreover, this option is "not really supported by
 * avr-libc, and should normally not be used" [1].
 *
 * [1] http://www.nongnu.org/avr-libc/user-manual/using_tools.html
 */
#if __INT_MAX__ != 32767
#error "Comiler option -mint8 isn't currently supported by Rvlm AVR."
#endif

// *** Integral types ***
typedef signed int    int8_t    __attribute__((__mode__(__QI__)));
typedef unsigned int  uint8_t   __attribute__((__mode__(__QI__)));
typedef signed int    int16_t   __attribute__((__mode__(__HI__)));
typedef unsigned int  uint16_t  __attribute__((__mode__(__HI__)));
typedef signed int    int32_t   __attribute__((__mode__(__SI__)));
typedef unsigned int  uint32_t  __attribute__((__mode__(__SI__)));
typedef signed int    int64_t   __attribute__((__mode__(__DI__)));
typedef unsigned int  uint64_t  __attribute__((__mode__(__DI__)));
typedef int8_t        int_least8_t;
typedef uint8_t       uint_least8_t;
typedef int16_t       int_least16_t;
typedef uint16_t      uint_least16_t;
typedef int32_t       int_least32_t;
typedef uint32_t      uint_least32_t;
typedef int64_t       int_least64_t;
typedef uint64_t      uint_least64_t;
typedef int8_t        int_fast8_t;
typedef uint8_t       uint_fast8_t;
typedef int16_t       int_fast16_t;
typedef uint16_t      uint_fast16_t;
typedef int32_t       int_fast32_t;
typedef uint32_t      uint_fast32_t;
typedef int64_t       int_fast64_t;
typedef uint64_t      uint_fast64_t;
typedef int64_t       intmax_t;
typedef uint64_t      uintmax_t;
typedef int16_t       size_t;
typedef int8_t        sig_atomic_t;
typedef int16_t       intptr_t;
typedef uint16_t      uintptr_t;
typedef intptr_t      ptrdiff_t;

// *** Types ranges ***
const int8_t          INT8_MAX          = static_cast<int8_t>(127LL);
const int8_t          INT8_MIN          = static_cast<int8_t>(-128LL);
const uint8_t         UINT8_MAX         = static_cast<uint8_t>(255ULL);
const int16_t         INT16_MAX         = static_cast<int16_t>(32767LL);
const int16_t         INT16_MIN         = static_cast<int16_t>(-32768LL);
const uint16_t        UINT16_MAX        = static_cast<uint16_t>(65535ULL);
const int32_t         INT32_MAX         = static_cast<int32_t>(2147483647LL);
const int32_t         INT32_MIN         = static_cast<int32_t>(-2147483648LL);
const uint32_t        UINT32_MAX        = static_cast<uint32_t>(4294967295ULL);
const int64_t         INT64_MAX         = static_cast<int64_t>(9223372036854775807LL);
const int64_t         INT64_MIN         = static_cast<int64_t>(-9223372036854775808ULL);
const uint64_t        UINT64_MAX        = static_cast<uint64_t>(18446744073709551615ULL);
const int_least8_t    INT_LEAST8_MAX    = INT8_MAX;
const int_least8_t    INT_LEAST8_MIN    = INT8_MIN;
const uint_least8_t   UINT_LEAST8_MAX   = UINT8_MAX;
const int_least16_t   INT_LEAST16_MAX   = INT16_MAX;
const int_least16_t   INT_LEAST16_MIN   = INT16_MIN;
const uint_least16_t  UINT_LEAST16_MAX  = UINT16_MAX;
const int_least32_t   INT_LEAST32_MAX   = INT32_MAX;
const int_least32_t   INT_LEAST32_MIN   = INT32_MIN;
const uint_least32_t  UINT_LEAST32_MAX  = UINT32_MAX;
const int_least64_t   INT_LEAST64_MAX   = INT64_MAX;
const int_least64_t   INT_LEAST64_MIN   = INT64_MIN;
const uint_least64_t  UINT_LEAST64_MAX  = UINT64_MAX;
const int_fast8_t     INT_FAST8_MAX     = INT8_MAX;
const int_fast8_t     INT_FAST8_MIN     = INT8_MIN;
const uint_fast8_t    UINT_FAST8_MAX    = UINT8_MAX;
const int_fast16_t    INT_FAST16_MAX    = INT16_MAX;
const int_fast16_t    INT_FAST16_MIN    = INT16_MIN;
const uint_fast16_t   UINT_FAST16_MAX   = UINT16_MAX;
const int_fast32_t    INT_FAST32_MAX    = INT32_MAX;
const int_fast32_t    INT_FAST32_MIN    = INT32_MIN;
const uint_fast32_t   UINT_FAST32_MAX   = UINT32_MAX;
const int_fast64_t    INT_FAST64_MAX    = INT64_MAX;
const int_fast64_t    INT_FAST64_MIN    = INT64_MIN;
const uint_fast64_t   UINT_FAST64_MAX   = UINT64_MAX;
const intptr_t        INTPTR_MAX        = INT16_MAX;
const intptr_t        INTPTR_MIN        = INT16_MIN;
const uintptr_t       UINTPTR_MAX       = UINT16_MAX;
const intmax_t        INTMAX_MAX        = INT64_MAX;
const intmax_t        INTMAX_MIN        = INT64_MIN;
const uintmax_t       UINTMAX_MAX       = UINT64_MAX;
const ptrdiff_t       PTRDIFF_MAX       = INT16_MAX;
const ptrdiff_t       PTRDIFF_MIN       = INT16_MIN;
const sig_atomic_t    SIG_ATOMIC_MAX    = INT8_MAX;
const sig_atomic_t    SIG_ATOMIC_MIN    = INT8_MIN;
const size_t          SIZE_MAX          = INT16_MAX;
