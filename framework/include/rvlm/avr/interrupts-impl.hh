#pragma once
#include <rvlm/avr/interrupts-fwd.hh>

namespace rvlm {
namespace avr {
namespace impl {

/// @todo Write documentation.
// HACK: Setting weak linkage mode lets the library stay headers-only.
// TODO: Make a macro for __attribute__((weak)), or not?
extern "C" volatile globalWeakInterruptsTable[] __attribute__((weak));

inline const InterruptHandler getInterruptHandler(
    const InterruptVector vector) {
    return globalWeakInterruptsTable[vector];
}

inline void setInterruptHandler(
    const InterruptVector vector,
    const InterruptHandler handler) {
    globalWeakInterruptsTable[vector] = handler;
}

inline void resetInterruptHandler(const InterruptVector vector) {
    globalWeakInterruptsTable[vector] = 0;
}

} // namespace impl
} // namespace avr
} // namespace rvlm
