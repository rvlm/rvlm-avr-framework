#pragma once
#include <rvlm/avr/bitops.hh>
#include <rvlm/avr/intrinsics.hh>

namespace rvlm {
namespace avr {

/**
 * Disables interrupts in a code scope.
 *
 * This class is useful for scope-based disabling of hardware interrupts.
 * Unlike @c cli() and @c sei() intrinsics, this class can be safely nested.
 * Let's consider the following example:
 * @code
 *     void f() {
 *         cli();
 *         g();
 *         ...; // (1)
 *         sti();
 *     }
 *
 *     void g() {
 *         cli();
 *         ...;
 *         sti();
 *     }
 * @endcode
 *
 * Here function @c g() disabled interrupts as well as @c f() does. But beeing
 * called with already disabled interupts, it enables them back too early, so
 * the code marked as (1) will be running with enabled interrupts, which is
 * obviously not the desired behavior. To make this code safe, one may rewrite
 * it like that:
 * @code
 *     void f() {
 *         ScopedInterruptsLock lock;
 *         g();
 *         ...; // (1)
 *     }
 *
 *     void g() {
 *         ScopedInterruptsLock lock;
 *         cli();
 *         ...;
 *     }
 * @endcode
 *
 * In the latter example the lock is obtained starting from declaration point
 * until the end of scope. A nested scope may be introduced with @c { and @c }
 * in case only a part of function should be locked. Alternatively, @c INTLOCK
 * macro can be used.
 *
 * @see intrinsics::cli
 * @see intrinsics::sei
 * @see INTLOCK
 */
class ScopedInterruptsLock {
private:
    // TODO: Need 'volatile'?
    volatile bool mSavedIntFlag;

public:

    /**
     * Creates class instance and enters protected block.
     * @see enterBlock
     */
    ScopedInterruptsLock() {
        enterBlock();
    }

    /**
     * Destroys class instance and leaves protected block.
     * @see leaveBlock
     */
    ~ScopedInterruptsLock() {
        leaveBlock();
    }

    /**
     * Enters protected block.
     * This method saves current state of global iterrupts flag for further
     * restoral and disables hardware interrupts. It also returns saved value
     * of interrupts flag.
     * @see leaveBlock
     */
    bool enterBlock() {
        uint8_t reg = rvlm::avr::registers::SREG;
        rvlm::avr::intrinsics::cli();
        mSavedIntFlag = rvlm::avr::isBitSet(reg);

        return mSavedIntFlag;
    }

    /**
     * Leaves protected block.
     * This method restores global iterrupts flag state which was saved by
     * @c enterBlock() method. It also returns saved value of interrupts flag.
     * @see enterBlock
     */
    bool leaveBlock() {
        if (mSavedIntFlag)
            rvlm::avr::intrinsics::sti();

        return mSavedIntFlag;
    }
};

} // namespace avr
} // namespace rvlm
