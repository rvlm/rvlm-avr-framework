#pragma once
#include <rvlm/avr/scoped-interrupts-lock.hh>

/**
 * @ingroup interrupts
 * @headerfile <rvlm/avr/intlock>
 * @hideinitializer
 * Disables interrupts for a nested scope.
 * This macro is used for temporary locking interrupts for a block of code, but
 * unlike @c cli and @c sei intrinsics, they can be safely nested. For example,
 * it can be something like this:
 * @code
 *     // ENABLED
 *     lock {
 *         // DISABLED
 *         lock {
 *             // DISABLED
 *         }
 *         // DISABLED
 *     }
 *     // ENABLED
 * @endcode
 * Note that this macro is for convenience only and potentially error-prone.
 * It is not recommended to use it in header files. Please, in those cases
 * use @c ScopedInterruptsLock instead.
 *
 * @see rvlm::avr::intrinsics::cli
 * @see rvlm::avr::intrinsics::sei
 * @see rvlm::avr::ScopedInterruptsLock
 */
#define lock \
    for (::rvlm::avr::ScopedInterruptsLock __rvlm_avr_lock_scopedIntLock,\
         bool __rvlm_avr_lock_continueLoopFlag = true;\
         __rvlm_avr_lock_continueLoopFlag;\
         __rvlm_avr_lock_continueLoopFlag = false)
