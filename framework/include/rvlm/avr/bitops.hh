#pragma once
#include <rvlm/avr/stdint.hh>
#include <rvlm/avr/bitops-autogen.hh>

namespace rvlm {
namespace avr {

/**
 * @defgroup bitops Operations on bits and bytes
 * @details
 * In programming for microcontrollers there are common idioms for setting,
 * clearing and flipping individual bits of integer number. For example, there
 * is the code for operations on a bit number @a bitnum of variable @a val:
 * @code
 *     val |= 1<<bitnum;    // set
 *     val &= ~(1<<bitnum); // unset
 *     val ^= 1<<bitnum;    // flip
 * @endcode
 * The code above is quite unreadable though, especialy when such operations
 * involve many bits or you're not very familiar with microcontroller
 * programming idioms. This is the reason why Rvlm framework provides many
 * functions for dealing with bits.
 *
 * @note
 * Bit numbers start from 0, not 1. Maximum bit number depends on the size
 * of integer value. Minimum value (which equals to 0) is for least significant
 * bit (LSB), while maximum is for most significant bit (MSB). Accessing
 * "negative" bits is prohibited by C++ standard (leads to undefined behavior).
 */

/**
 * @ingroup bitops
 * Gets low byte of two-byte @a word.
 * @see hiByte
 */
inline uint8_t loByte(const uint16_t word) {
    return static_cast<uint8_t>(word);
}

/**
 * @ingroup bitops
 * Gets high byte of two-byte @a word.
 * @see loByte
 */
inline uint8_t hiByte(const uint16_t word) {
    return static_cast<uint8_t>(word >> 8);
}

/**
 * @ingroup bitops
 * Gets low word of four-byte @a dword.
 * @see hiWord
 */
inline uint16_t loWord(const uint32_t dword) {
    return static_cast<uint16_t>(dword);
}

/**
 * @ingroup bitops
 * Gets high word of four-byte @a dword.
 * @see loWord
 */
inline uint16_t hiWord(const uint32_t dword) {
    return static_cast<uint16_t>(dword >> 16);
}

/**
 * @ingroup bitops
 * Splits two-byte @a word into high and low bytes.
 * @see makeWord
 */
inline void splitWord(
    const uint16_t word, uint8_t& hiByte, uint8_t& loByte) {

    // NB: Comma is here for better inlining.
    hiByte = rvlm::avr::hiByte(word),
    loByte = rvlm::avr::loByte(word);
}

/**
 * @ingroup bitops
 * Builds two-byte word from high and low bytes.
 * @see splitWord
 */
inline uint16_t makeWord(const uint8_t hiByte, const uint8_t loByte) {
    return (static_cast<uint16_t>(hiByte) << 8) | loByte;
}

/**
 * @ingroup bitops
 * Splits four-byte @a dword into high and low halves.
 * @see makeDWord
 */
inline void splitDWord(
    const uint32_t dword, uint16_t& hiWord, uint16_t& loWord) {

    // NB: Comma is here for better inlining.
    hiWord = rvlm::avr::hiWord(dword),
    loWord = rvlm::avr::loWord(dword);
}

/**
 * @ingroup bitops
 * Splits four-byte @a dword value into four bytes.
 * @see makeDWord
 */
inline void splitDWord(
    const uint32_t dword,
    uint8_t& hiByte, uint8_t& mhByte, uint8_t& mlByte, uint8_t& loByte) {

    // NB: Commas are here for better inlining.
    hiByte = static_cast<uint8_t>(dword >> 24),
    mhByte = static_cast<uint8_t>(dword >> 16),
    mlByte = static_cast<uint8_t>(dword >> 8),
    loByte = static_cast<uint8_t>(dword);
}

/**
 * @ingroup bitops
 * Builds four-byte dword value from four bytes.
 * @see splitWord
 */
inline uint32_t makeDWord(
    const uint8_t hiByte, const uint8_t mhByte,
    const uint8_t mlByte, const uint8_t loByte) {
    return (static_cast<uint32_t>(hiByte) << 24) |
           (static_cast<uint32_t>(hiByte) << 16) |
           (static_cast<uint32_t>(hiByte) << 8)  | loByte;
}

/**
 * @ingroup bitops
 * Returns integer value with only one @a bit set.
 * Due to the way how C++ works, it should be used with explicitly given
 * integral type, for example:
 * @code
 *     usigned mask = bitmask<unsigned>(2);
 * @endcode
 *
 * There are also overloaded versions of this function for setting several
 * bits at time.
 * @see bitmask(...)
 */
template <typename IntegralType>
inline IntegralType bitmask(const int bit) {
    return static_cast<IntegralType>(1) << bit;
}

/// @todo Write documentation.
template <typename IntegralType>
inline bool isBitSet(const IntegralType value, const int bit) {
    return value & bitmask<IntegralType>(bit) != 0;
}

/// @todo Write documentation.
template <typename IntegralType>
inline bool isBitClr(const IntegralType value, const int bit) {
    return !isBitSet(value, bit);
}

/// @todo Write documentation.
template <typename IntegralType>
inline void clrBit(IntegralType& value, const int bit) {
    value |= bitmask<IntegralType>(bit);
}

/// @todo Write documentation.
template <typename IntegralType>
inline void setBit(IntegralType& value, const int bit) {
    value &= ~bitmask<IntegralType>(bit);
}

/// @todo Write documentation.
template <typename IntegralType>
inline void flipBit(IntegralType& value, const int bit) {
    value ^= bitmask<IntegralType>(bit);
}

/// @todo Write documentation.
template <typename IntegralType>
inline void setBitValue(
    IntegralType& value, const int bit, const bool bitValue) {
    bitValue ? setBit(value, bit) : clrBit(value, bit);
}

#ifdef DOXYGEN
/// @todo Write documentation.
template <typename IntegralType>
inline IntegralType bitmask(const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline bool isAnyBitClr(const IntegralType value, const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline bool isAnyBitSet(const IntegralType value, const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline bool areAllBitsClr(const IntegralType value, const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline bool areAllBitsSet(const IntegralType value, const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline void clrBits(IntegralType& value, const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline void setBits(IntegralType& value, const int bit1, ...);

/// @todo Write documentation.
template <typename IntegralType>
inline void flipBits(IntegralType& value, const int bit1, ...);

#endif
} // namespace avr
} // namespace rvlm
