#pragma once

// WARNING:
// This file contains generated code!
// Please do not modify its contents since all your changes will be lost
// on regeneration. Modify generate-select-header.pl file instead.
//
// NOTE:
// This file's lines length is not restricted to 80 columns. Do not try
// to beautify it out of pure perfectionism.

#ifndef __MCU__
#error "You must define __MCU__ before including this file."
#else
#if 0
#elif __MCU__ == "atmega128"
#include <rvlm/avr/registers/atmega128.pm>
#else
#error "Unsupported value of __MCU__."
#endif
#endif
