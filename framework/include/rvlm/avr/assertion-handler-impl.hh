#pragma once
#include <rvlm/avr/compiler.hh>
#include <rvlm/avr/assertion-handler-fwd.hh>
#include <rvlm/avr/scoped-interrupts-lock.hh>

namespace rvlm {
namespace avr {
namespace impl {


inline void defaultAssertHandler(const char* message) {
    rvlm::avr::intrinsics::breakOp();
    while (true)
        ;
}

inline const AssertionHandler& getAssertHandler() {
    ScopedInterruptsLock intlock;
    return globalWeakAssertionHandler;
}

inline void setAssertHandler(const AssertionHandler& handler) {
    ScopedInterruptsLock intlock;
    globalWeakAssertionHandler = handler;
}

RVLM_AVR_WEAK_LINKAGE
AssertionHandler globalWeakAssertionHandler = &defaultAssertHandler;

} // namespace impl
} // namespace avr
} // namespace rvlm
