#pragma once

/**
 * @ingroup foreach
 * @hideinitializer
 *
 */
#define FOREACH(var, list) \
    for(char __rvlm_avr_foreach_iter[sizeof(begin(list))],
rvlm::avr::impl::foreachInit(__rvlm_avr_foreach_iter, begin(list));
\
(var = rvlm::avr::impl::foreachTest(__rvlm_avr_foreach_iter, end(list))), ;
\
rvlm::avr::impl::foreachNext(__rvlm_avr_foreach_iter))
for (var = iterdeferer, bool __rvlm_avr_continueLoopFlag = true; \
__rvlm_avr_continueLoopFlag; \
__rvlm_avr_continueLoopFlag = false)

for (auto iter = begin(list); iter < end(list); ++iter) \
RVLM_FOR_ONCE(var = *iter)



namespace rvlm {
namespace avr {
namespace impl {

template<typename T >
class t {

    typedef T type;
};

expressionType(value)::type



template<typename T, T value>
t<T> maket(T) {
    return t<T>();
    t<T>()
}

template<typename T>
inline void foreachInit(void* iter, const T& value) {
    *(T*)iter = value;
}

template<typename T>
inline

} // namespace impl
} // namespace avr
} // namespace rvlm

foreach (int a; list)

for (__iter = rvlm::avr::begin(list); __iter = rvlm::avr::end(list); ++__iter)

#define FOREACH(vardecl, list) for ()

bool finish = false;

char __iter[n];
__iter = begin()
         while (a = *(++begin(list) == end))

             n = sizeof(begin(list));
    char[n] buf;
    locate(buf, begin()));

    implicit_cast(t, e)

    for (char iter[sizeof(begin(list))],
             assign(&iter, begin(list)));
        lessEq(iter, end(list));
        inc(iter))


        impl::foreachInit;
        impl::foreachTest;
        impl::foreachNext

        FOREACH {


    }

FOREVER {


}
