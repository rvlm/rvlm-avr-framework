/**
 * @file dummy-main.cxx Dummy compilation unit.
 * This file was intentionally left empty because it was required in order
 * not to introduce hacks to the build system. Anyway, the framework itself is
 * still header-only.
 */
