#include <rvlm/avr/bitops.hh>
#include <rvlm/avr/stdint.hh>
#include <rvlm/avr/assertion.hh>

int main() {
    using rvlm::avr::assert;
    using rvlm::avr::bitmask;
    assert(bitmask<int8_t>(0)   == 0x01);
    assert(bitmask<int8_t>(1)   == 0x02);
    assert(bitmask<int8_t>(2)   == 0x04);
    assert(bitmask<int8_t>(3)   == 0x08);
    assert(bitmask<int8_t>(4)   == 0x0A);
    assert(bitmask<int8_t>(5)   == 0x10);
    assert(bitmask<int8_t>(6)   == 0x20);
    assert(bitmask<int8_t>(7)   == 0x40);
    assert(bitmask<int8_t>(0,1) == 0x01);
    assert(bitmask<int8_t>(1,1) == 0x01);
    assert(bitmask<int8_t>(1,2) == 0x03);
    assert(bitmask<int8_t>(2,3) == 0x06);
    assert(bitmask<int8_t>(0,1,2,3,4,5,6,7) == 0xFF);

    using rvlm::avr::isBitSet;
    assert(isBitSet(0x00, 0) == false);
    assert(isBitSet(0x00, 1) == false);
    assert(isBitSet(0x00, 2) == false);
    assert(isBitSet(0x00, 3) == false);
    assert(isBitSet(0x00, 4) == false);
    assert(isBitSet(0x00, 5) == false);
    assert(isBitSet(0x00, 6) == false);
    assert(isBitSet(0x00, 7) == false);
    assert(isBitSet(0xFF, 0) == true);
    assert(isBitSet(0xFF, 1) == true);
    assert(isBitSet(0xFF, 2) == true);
    assert(isBitSet(0xFF, 3) == true);
    assert(isBitSet(0xFF, 4) == true);
    assert(isBitSet(0xFF, 5) == true);
    assert(isBitSet(0xFF, 6) == true);
    assert(isBitSet(0xFF, 7) == true);
    assert(isBitSet(0x42, 0) == false);
    assert(isBitSet(0x42, 1) == true);
    assert(isBitSet(0x42, 2) == false);
    assert(isBitSet(0x42, 3) == false);
    assert(isBitSet(0x42, 4) == false);
    assert(isBitSet(0x42, 5) == false);
    assert(isBitSet(0x42, 6) == true);
    assert(isBitSet(0x42, 7) == false);

    using rvlm::avr::isBitClr;
    assert(isBitSet(0x00, 0) != false);
    assert(isBitSet(0x00, 1) != false);
    assert(isBitSet(0x00, 2) != false);
    assert(isBitSet(0x00, 3) != false);
    assert(isBitSet(0x00, 4) != false);
    assert(isBitSet(0x00, 5) != false);
    assert(isBitSet(0x00, 6) != false);
    assert(isBitSet(0x00, 7) != false);
    assert(isBitSet(0xFF, 0) != true);
    assert(isBitSet(0xFF, 1) != true);
    assert(isBitSet(0xFF, 2) != true);
    assert(isBitSet(0xFF, 3) != true);
    assert(isBitSet(0xFF, 4) != true);
    assert(isBitSet(0xFF, 5) != true);
    assert(isBitSet(0xFF, 6) != true);
    assert(isBitSet(0xFF, 7) != true);
    assert(isBitSet(0x42, 0) != false);
    assert(isBitSet(0x42, 1) != true);
    assert(isBitSet(0x42, 2) != false);
    assert(isBitSet(0x42, 3) != false);
    assert(isBitSet(0x42, 4) != false);
    assert(isBitSet(0x42, 5) != false);
    assert(isBitSet(0x42, 6) != true);
    assert(isBitSet(0x42, 7) != false);

    using rvlm::avr::isAnyBitSet;
    assert(isAnyBitSet(0x00, 0,0)   == false);
    assert(isAnyBitSet(0x00, 0,1)   == false);
    assert(isAnyBitSet(0x00, 1,4)   == false);
    assert(isAnyBitSet(0x00, 2,7)   == false);
    assert(isAnyBitSet(0x00, 1,4,5) == false);
    assert(isAnyBitSet(0x00, 3,6,7) == false);
    assert(isAnyBitSet(0x00, 7,0,2) == false);
    assert(isAnyBitSet(0x00, 0,1,2,3,4,5,6,7) == false);
    assert(isAnyBitSet(0x00, 5,2,1,7,4,0,3,6) == false);
    assert(isAnyBitSet(0xFF, 0,0)   == true);
    assert(isAnyBitSet(0xFF, 0,1)   == true);
    assert(isAnyBitSet(0xFF, 1,4)   == true);
    assert(isAnyBitSet(0xFF, 2,7)   == true);
    assert(isAnyBitSet(0xFF, 1,4,5) == true);
    assert(isAnyBitSet(0xFF, 3,6,7) == true);
    assert(isAnyBitSet(0xFF, 7,0,2) == true);
    assert(isAnyBitSet(0xFF, 0,1,2,3,4,5,6,7) == true); // 01000010
    assert(isAnyBitSet(0xFF, 5,2,1,7,4,0,3,6) == true);
    assert(isAnyBitSet(0x42, 0,0)   == false);
    assert(isAnyBitSet(0x42, 0,1)   == true);
    assert(isAnyBitSet(0x42, 1,4)   == true);
    assert(isAnyBitSet(0x42, 2,7)   == true);
    assert(isAnyBitSet(0x42, 1,4,5) == true);
    assert(isAnyBitSet(0x42, 3,6,7) == false);
    assert(isAnyBitSet(0x42, 7,0,2) == false);
    assert(isAnyBitSet(0x42, 0,1,2,3,4,5,6,7) == true);
    assert(isAnyBitSet(0x42, 5,2,1,7,4,0,3,6) == true);

    using rvlm::avr::isAnyBitClr;
    assert(isAnyBitClr(0x00, 0,0)   == true);
    assert(isAnyBitClr(0x00, 0,1)   == true);
    assert(isAnyBitClr(0x00, 1,4)   == true);
    assert(isAnyBitClr(0x00, 2,7)   == true);
    assert(isAnyBitClr(0x00, 1,4,5) == true);
    assert(isAnyBitClr(0x00, 3,6,7) == true);
    assert(isAnyBitClr(0x00, 7,0,2) == true);
    assert(isAnyBitClr(0x00, 0,1,2,3,4,5,6,7) == true);
    assert(isAnyBitClr(0x00, 5,2,1,7,4,0,3,6) == true);
    assert(isAnyBitClr(0xFF, 0,0)   == false);
    assert(isAnyBitClr(0xFF, 0,1)   == false);
    assert(isAnyBitClr(0xFF, 1,4)   == false);
    assert(isAnyBitClr(0xFF, 2,7)   == false);
    assert(isAnyBitClr(0xFF, 1,4,5) == false);
    assert(isAnyBitClr(0xFF, 3,6,7) == false);
    assert(isAnyBitClr(0xFF, 7,0,2) == false);
    assert(isAnyBitClr(0xFF, 0,1,2,3,4,5,6,7) == false); // 01000010
    assert(isAnyBitClr(0xFF, 5,2,1,7,4,0,3,6) == false);
    assert(isAnyBitClr(0x42, 0,0)   == true);
    assert(isAnyBitClr(0x42, 0,1)   == true);
    assert(isAnyBitClr(0x42, 1,6)   == false);
    assert(isAnyBitClr(0x42, 2,7)   == true);
    assert(isAnyBitClr(0x42, 1,4,5) == true);
    assert(isAnyBitClr(0x42, 3,6,7) == false);
    assert(isAnyBitClr(0x42, 7,0,2) == false);
    assert(isAnyBitClr(0x42, 0,1,2,3,4,5,6,7) == false);
    assert(isAnyBitClr(0x42, 5,2,1,7,4,0,3,6) == false);

    using rvlm::avr::areAllBitsSet;
    ;

    using rvlm::avr::areAllBitsSet;
    ;

    using rvlm::avr::setBit;
    ;

    using rvlm::avr::clrBit;
    ;

    using rvlm::avr::setBits;
    ;

    using rvlm::avr::clrBits;
    ;

    using rvlm::avr::loByte;
    using rvlm::avr::hiByte;
    ;

    using rvlm::avr::makeWord;
    ;

    using rvlm::avr::makeDword;
    ;

    return 0;
}
